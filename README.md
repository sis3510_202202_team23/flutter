# Flutter

First of all, change to develop branch.
Open the repo project in vs 
Check down right the device to use (Chrome, edge, windows or the android studio device)
Choose the android studio device
Open lib/main.dart and then right click> Run Without Debugging 
Wait
You are ready to test

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/sis3510_202202_team23/flutter.git
git branch -M main
git push -uf origin main
```
