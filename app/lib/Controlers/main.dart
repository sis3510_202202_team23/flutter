import 'package:app/components/CarCard.dart';
import 'package:app/components/ServiceCard2.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:app/dao/singletonDTO.dart';

import '../constants.dart';

class MainControler extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }

  static getTextEmail() {
    return "¡Hola, ${SingletonDAO.getUser()!.email}!";
  }

  static Future getCarros() async {
    return SingletonDAO.getCarros();
  }

  static Future getServiciosFavoritos() async {
    return SingletonDAO.getServiciosFavoritos();
  }

  static Future getServicioOrdenados() async {
    return SingletonDAO.getServicioOrdenados();
  }

  static construirServiciosFav() {
    return FutureBuilder(
      future: MainControler.getServiciosFavoritos(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data == null) {
            return Text('no data');
          } else {
            return Container(
              margin: const EdgeInsets.only(left: 15, bottom: 10),
              height: 150.0,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  itemBuilder: (_, index) {
                    return ServiceCard2(
                      nombreservicio_: snapshot.data[index]["servicetype"],
                      nombrefoto_: "servicio1",
                      recomended: false,
                      color_: kSecondaryLightColor,
                    );
                  }),
            );
          }
        } else if (snapshot.connectionState == ConnectionState.none) {
          return Text('Error'); // error
        } else {
          return CircularProgressIndicator(); // loading
        }
      },
    );
  }

  static construirServiciosOrdenados() {
    return FutureBuilder(
      future: MainControler.getServicioOrdenados(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data == null) {
            return Text('no data');
          } else {
            return Container(
              margin: const EdgeInsets.only(left: 15, bottom: 10),
              height: 150.0,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  shrinkWrap: true,
                  itemCount: snapshot.data.length,
                  itemBuilder: (_, index) {
                    var colorNow = kSecondaryLightColor;
                    var isrecomended = false;
                    if (index == 0) {
                      colorNow = kPrimaryColor;
                      isrecomended = true;
                    }
                    return ServiceCard2(
                      nombreservicio_: snapshot.data[index]["name"],
                      nombrefoto_: "servicio1",
                      color_: colorNow,
                      recomended: isrecomended,
                    );
                  }),
            );
          }
        } else if (snapshot.connectionState == ConnectionState.none) {
          return Text('Error'); // error
        } else {
          return CircularProgressIndicator(); // loading
        }
      },
    );
  }

  static construirCarsRows() {
    return FutureBuilder(
      future: MainControler.getCarros(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data == null) {
            return Text('no data');
          } else {
            return Container(
                padding: EdgeInsets.only(left: 15.0),
                height: 220.0,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: snapshot.data.length,
                    itemBuilder: (_, index) {
                      return CarCard(
                        name_: snapshot.data[index]["name"],
                        plate_: snapshot.data[index]["plate"],
                      );
                    }));
          }
        } else if (snapshot.connectionState == ConnectionState.none) {
          return Text('Error'); // error
        } else {
          return CircularProgressIndicator(); // loading
        }
      },
    );
  }
}
