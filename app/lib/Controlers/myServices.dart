import 'dart:math';

import 'package:app/components/NotificationTextContainer.dart';
import 'package:app/model/service.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'package:app/dao/singletonDTO.dart';

class MyServicesControler extends StatelessElement {
  MyServicesControler(super.widget);

  static Future createService(
    String _idCar,
    String _idUser,
    String _serviceType,
    String _state,
  ) async {
    final service = ServiceModel(
      idCar: _idCar,
      idUser: _idUser,
      serviceType: _serviceType,
      state: _state,
    );
    var collection = FirebaseFirestore.instance.collection('services');
    await collection
        .add(service.toMap()) // <-- Your data
        .then((_) => print('Added'))
        .catchError((error) => print('Add failed: $error'));
  }

  static MyServicesData() {
    return Container(
      child: FutureBuilder(
        future: SingletonDAO.getMyServices(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data == null) {
              return Text('no data');
            } else {
              return Container(
                  margin: const EdgeInsets.symmetric(vertical: 20.0, horizontal: 20.0),
                  height: min(snapshot.data.length * 300.0, 600.0),
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: snapshot.data.length,
                      itemBuilder: (_, index) {
                        return NotificationTextContainer(
                            service_status: snapshot.data[index]["state"],
                            fecha: "Ayer",
                            service_type : snapshot.data[index]["servicetype"]
                        );
                      }
                    )
                  );
            }
          } else if (snapshot.connectionState == ConnectionState.none) {
            return Text('Error'); // error
          } else {
            return CircularProgressIndicator(); // loading
          }
        },
      ),
    );
  }
}