import 'dart:math';

import 'package:app/components/CarCard.dart';
import 'package:app/model/car.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:app/dao/singletonDTO.dart';

import '../components/CarBlock.dart';

class MyCarsControler extends StatelessElement {
  MyCarsControler(super.widget);

  static Future createCar(
    String _city,
    String _country,
    String _description,
    String _model,
    String _name,
    String _plate,
  ) async {
    final car = CarModel(
        city: _city,
        country: _country,
        description: _description,
        model: _model,
        name: _name,
        plate: _plate,
        user: SingletonDAO.getUser().uid);
    var collection = FirebaseFirestore.instance.collection('cars');
    await collection
        .add(car.toMap()) // <-- Your data
        .then((_) => print('Added'))
        .catchError((error) => print('Add failed: $error'));
  }

  static CarRow() {
    return Container(
      child: FutureBuilder(
        future: SingletonDAO.getCarros(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data == null) {
              return Text('no data');
            } else {
              return Container(
                  margin: const EdgeInsets.symmetric(vertical: 20.0),
                  height: min(snapshot.data.length * 300.0, 450.0),
                  child: ListView.builder(
                      scrollDirection: Axis.vertical,
                      shrinkWrap: true,
                      itemCount: snapshot.data.length,
                      itemBuilder: (_, index) {
                        return CarBlock(
                            name_: snapshot.data[index]["name"],
                            plate_: snapshot.data[index]["plate"],
                            brand_: "",
                            model_: snapshot.data[index]["model"],
                            year_: "",
                            mileage_: "");
                      }));
            }
          } else if (snapshot.connectionState == ConnectionState.none) {
            return Text('Error'); // error
          } else {
            return CircularProgressIndicator(); // loading
          }
        },
      ),
    );
  }
}
