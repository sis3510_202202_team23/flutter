// ignore_for_file: deprecated_member_use

import 'package:app/Screens/Login/login_screen.dart';
import 'package:app/Screens/SignUp/signup_screen.dart';
import 'package:app/components/CarCard.dart';
import 'package:app/dao/services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import '../../../components/LogoImage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../../../components/ServiceCard2.dart';
import 'package:app/Screens/home/components/background.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../constants.dart';
import 'package:app/Controlers/main.dart';

class Main_Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  SingleChildScrollView(
                      padding: EdgeInsets.only(left: 30.0),
                      child: LogoImage(height_: 100, width_: 100)),
                ],
              ),
              Container(
                margin:
                    EdgeInsets.only(left: 10.0, right: 20.0, bottom: 0, top: 0),
                decoration: BoxDecoration(
                    border: Border.all(color: Colors.black),
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(15))),
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Pico y Placa hoy:",
                          textAlign: TextAlign.center,
                          style: GoogleFonts.raleway(
                              textStyle: Theme.of(context).textTheme.headline4,
                              fontSize: 18,
                              color: Colors.black),
                        ),
                        Text(
                          fechaPar(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.raleway(
                              textStyle: Theme.of(context).textTheme.headline4,
                              fontSize: 18,
                              color: Colors.black,
                              fontWeight: FontWeight.w600),
                        )
                      ]),
                ),
              )
            ],
          ),
          Container(
            padding: EdgeInsets.only(left: 15.0, bottom: 10),
            child: Text(
              MainControler.getTextEmail(),
              style: GoogleFonts.raleway(
                  textStyle: Theme.of(context).textTheme.headline4,
                  fontSize: 25,
                  color: Colors.white,
                  fontWeight: FontWeight.w300),
            ),
          ),
          Container(
            padding: EdgeInsets.only(left: 15.0),
            child: Text(
              "Mi Garaje",
              style: GoogleFonts.raleway(
                  textStyle: Theme.of(context).textTheme.headline4,
                  fontSize: 25,
                  color: Colors.white,
                  fontWeight: FontWeight.w800),
            ),
          ),
          CarsRow(context),
          Container(
            padding: EdgeInsets.only(left: 15.0, bottom: 10),
            child: Text(
              "Servicios disponibles",
              style: GoogleFonts.raleway(
                  textStyle: Theme.of(context).textTheme.headline4,
                  fontSize: 25,
                  color: Colors.white,
                  fontWeight: FontWeight.w800),
            ),
          ),
          ServiciosOrdenados(context),
          Container(
            padding: EdgeInsets.only(left: 15.0, bottom: 10),
            child: Text(
              "Mis Favoritos",
              style: GoogleFonts.raleway(
                  textStyle: Theme.of(context).textTheme.headline4,
                  fontSize: 25,
                  color: Colors.white,
                  fontWeight: FontWeight.w800),
            ),
          ),
          ServiciosFavoritos(),
        ],
      ),
    );
  }

  ServiciosFavoritos() {
    return Container(
      child: MainControler.construirServiciosFav(),
    );
  }

  ServiciosOrdenados(context) {
    return Container(child: MainControler.construirServiciosOrdenados());
  }

  CarsRow(context) {
    return Container(
      child: MainControler.construirCarsRows(),
    );
  }

  Container car_card(context, name_, plate_, brand_, model_, year_, mileage_) {
    return Container(
      width: 150.0,
      color: kSecondaryLightColor,
      margin: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      padding: const EdgeInsets.all(8),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              PicoYPlacaWarning2(placa: plate_),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.black),
                        color: Colors.yellow,
                        borderRadius: BorderRadius.all(Radius.circular(5))),
                    child: Text(
                      "$plate_",
                      textAlign: TextAlign.end,
                      style: GoogleFonts.raleway(
                        textStyle: Theme.of(context).textTheme.headline4,
                        fontSize: 15,
                        color: Colors.black,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                  ),
                  Image.asset("assets/images/carrito2.png",
                      height: 100, width: 100),
                  Text(
                    "$name_",
                    textAlign: TextAlign.end,
                    style: GoogleFonts.raleway(
                        textStyle: Theme.of(context).textTheme.headline4,
                        fontSize: 20,
                        color: Colors.white,
                        fontWeight: FontWeight.w700),
                  )
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }

  Container add_card(context) {
    return Container(
      width: 150.0,
      color: kSecondaryLightColor,
      margin: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      padding: const EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "+",
                textAlign: TextAlign.end,
                style: GoogleFonts.raleway(
                  textStyle: Theme.of(context).textTheme.headline4,
                  fontSize: 75,
                  color: Colors.white,
                ),
              ),
              Text(
                "Agregar",
                textAlign: TextAlign.end,
                style: GoogleFonts.raleway(
                    textStyle: Theme.of(context).textTheme.headline4,
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.w700),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class ServiciosFavoritos extends StatefulWidget {
  @override
  _ServiciosFavoritosState createState() => _ServiciosFavoritosState();
}

class _ServiciosFavoritosState extends State<ServiciosFavoritos> {
  Future getServiciosFavoritos() async {
    final user = FirebaseAuth.instance.currentUser;
    var firestore = FirebaseFirestore.instance;
    QuerySnapshot querySnapshot = await firestore
        .collection("services_clients")
        .where("idUser", isEqualTo: user!.uid)
        //.orderBy("amount").withConverter(fromFirestore: fromFirestore, toFirestore: toFirestore)
        .get();
    final documents = querySnapshot.docs;
    //código para organizar el documento
    return documents;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
        future: getServiciosFavoritos(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data == null) {
              return Text('no data');
            } else {
              return Container(
                margin: const EdgeInsets.only(left: 15, bottom: 10),
                height: 150.0,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemCount: snapshot.data.length,
                    itemBuilder: (_, index) {
                      return ServiceCard2(
                        nombreservicio_: snapshot.data[index]["servicetype"],
                        nombrefoto_: "servicio1",
                        recomended: false,
                        color_: kSecondaryLightColor,
                      );
                    }),
              );
            }
          } else if (snapshot.connectionState == ConnectionState.none) {
            return Text('Error'); // error
          } else {
            return CircularProgressIndicator(); // loading
          }
        },
      ),
    );
  }
}

class PicoYPlacaWarning2 extends StatelessWidget {
  final String placa;
  const PicoYPlacaWarning2({Key? key, required this.placa}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    int hoy = DateTime.now().day;
    int ultimoDigito = int.parse(placa.substring(placa.length - 1));
    if (ultimoDigito % 2 == hoy % 2) {
      return Container(
        child: Text(
          "Pico y Placa",
          style: GoogleFonts.raleway(
              textStyle: Theme.of(context).textTheme.headline4,
              fontSize: 15,
              color: Colors.red,
              fontWeight: FontWeight.w700),
        ),
      );
    } else {
      return Container(
        child: Text(
          "   ",
          style: GoogleFonts.raleway(
              textStyle: Theme.of(context).textTheme.headline4,
              fontSize: 15,
              color: Colors.red,
              fontWeight: FontWeight.w700),
        ),
      );
    }
  }
}

String fechaPar() {
  int hoy = DateTime.now().day;
  String respuesta = '';
  if (hoy % 2 == 0) {
    respuesta = '0,2,4,6,8';
  } else {
    respuesta = '1,3,5,7,9';
  }
  return respuesta;
}
