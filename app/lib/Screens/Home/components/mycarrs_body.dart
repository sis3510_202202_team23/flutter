// ignore_for_file: deprecated_member_use
import 'dart:io';
import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:app/components/CarImage.dart';

import 'package:app/Screens/home/components/background.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../constants.dart';

import 'package:app/components/CarBlock.dart';
import 'package:app/model/car.dart';

import 'package:image_picker/image_picker.dart';
import 'package:app/Controlers/mycars.dart';

class Cars_Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 60),
            child: Texto(context, "Mi Garaje", 30.0, kPrimaryColor,
                FontWeight.w800, TextAlign.center),
          ),
          MyCarsControler.CarRow(),
          GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                          title: Text("Registra tu carro"),
                          content: Formulario(),
                          actions: [
                            TextButton(
                                child: Text("Cancel"),
                                onPressed: () => Navigator.pop(context))
                          ],
                        ));
              },
              child: add_card(context))
        ],
      ),
    );
  }

  SizedBox Texto(
      context, String texto_, tamanio_, color_, fontWeight_, align_) {
    return SizedBox(
        width: double.infinity,
        child: Text(
          texto_,
          textAlign: align_,
          style: GoogleFonts.raleway(
              textStyle: Theme.of(context).textTheme.headline4,
              fontSize: tamanio_,
              color: color_,
              fontWeight: fontWeight_),
        ));
  }

  Container add_card(context) {
    return Container(
      width: 150.0,
      color: kSecondaryLightColor,
      margin: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
      padding: const EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "+",
                textAlign: TextAlign.end,
                style: GoogleFonts.raleway(
                  textStyle: Theme.of(context).textTheme.headline4,
                  fontSize: 75,
                  color: Colors.white,
                ),
              ),
              Text(
                "Agregar",
                textAlign: TextAlign.end,
                style: GoogleFonts.raleway(
                    textStyle: Theme.of(context).textTheme.headline4,
                    fontSize: 20,
                    color: Colors.white,
                    fontWeight: FontWeight.w700),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class Formulario extends StatefulWidget {
  @override
  FormularioState createState() => FormularioState();
}

class FormularioState extends State<Formulario> {
  final controllerCity = TextEditingController();
  final controllerCountry = TextEditingController();
  final controllerDescription = TextEditingController();
  final controllerModel = TextEditingController();
  final controllerName = TextEditingController();
  final controllerPlate = TextEditingController();

  final user = FirebaseAuth.instance.currentUser;

  File? image;

  Future pickImage() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (image == null) return;

      final imageTemporary = File(image.path);
      setState(() => this.image = imageTemporary);
    } on PlatformException catch (e) {
      print('Failed to  pick image: $e');
    }
  }

  Future takeImage() async {
    try {
      final image = await ImagePicker().pickImage(source: ImageSource.camera);
      if (image == null) return;

      final imageTemporary = File(image.path);
      setState(() => this.image = imageTemporary);
    } on PlatformException catch (e) {
      print('Failed to  pick image: $e');
    }
  }

  @override
  Widget build(BuildContext context) => Container(
      height: 350.0,
      width: 300.0,
      child: ListView(
        shrinkWrap: true,
        children: [
          image != null
              ? Image.file(image!, width: 160, height: 160)
              : FlutterLogo(size: 160),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.red),
                ),
                onPressed: () => pickImage(),
                child: Text("Pick Gallery"),
              ),
              SizedBox(
                width: 20, //<-- SEE HERE
              ),
              ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.red),
                ),
                onPressed: () => takeImage(),
                child: Text("Pick Camera"),
              ),
            ],
          ),
          TextField(
            controller: controllerCity,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'city',
            ),
          ),
          TextField(
            controller: controllerCountry,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'country',
            ),
          ),
          TextField(
            controller: controllerDescription,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'description',
            ),
          ),
          TextField(
            controller: controllerModel,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'model',
            ),
          ),
          TextField(
            controller: controllerPlate,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'plate',
            ),
          ),
          TextField(
            controller: controllerName,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'name',
            ),
          ),
          ElevatedButton(
              child: Text("create"),
              onPressed: () {
                MyCarsControler.createCar(
                  controllerCity.text,
                  controllerCity.text,
                  controllerDescription.text,
                  controllerModel.text,
                  controllerName.text,
                  controllerPlate.text,
                );
                Navigator.pop(context);
              }
            )
        ],
      )
    );
}
