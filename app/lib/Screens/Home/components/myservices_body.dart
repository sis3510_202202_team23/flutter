// ignore_for_file: deprecated_member_use

import 'dart:math';

import 'package:app/components/NotificationTextContainer.dart';
import 'package:app/dao/singletonDTO.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:app/Screens/home/components/background.dart';
import 'package:google_fonts/google_fonts.dart';

import 'package:app/Controlers/myServices.dart';

class Services_Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: size.height * 0.1),
          Text(
            "Servicio Actual",
            style: GoogleFonts.raleway(
                textStyle: Theme.of(context).textTheme.headline4,
                fontSize: 25,
                color: Colors.white),
          ),
          MyServicesControler.MyServicesData(),
        ],
      ),
    );
  }
}


