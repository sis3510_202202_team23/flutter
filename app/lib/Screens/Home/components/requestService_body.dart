// ignore_for_file: deprecated_member_use

import 'package:app/Screens/Login/login_screen.dart';
import 'package:app/Screens/SignUp/signup_screen.dart';
import 'package:app/components/FinalRequestTotal.dart';
import 'package:app/components/RoundedButton.dart';
import 'package:app/components/checkerconductoralfred.dart';
import 'package:app/components/serviciosseleccionados.dart';
import 'package:app/dao/singletonDTO.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:app/constants.dart';

import 'package:app/Screens/home/components/background.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:selectable_list/selectable_list.dart';

class RequestService_Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          //Formulario(),
          SizedBox(height: size.height * 0.1),
          Text(
            "Solicitar Servicio",
            style: GoogleFonts.raleway(
                textStyle: Theme.of(context).textTheme.headline4,
                fontSize: 20,
                color: Colors.white),
          ),
          SizedBox(height: size.height * 0.1),
          VehiculoService(info_carro: "Vehiculo: Mi carro", placa: "AXC456"),
          Row(children: [
            Padding(
              padding: EdgeInsets.only(left: 40, top: 20),
              child: Text(
                "Dirección de Recogida:",
                textAlign: TextAlign.end,
                style: GoogleFonts.raleway(
                  textStyle: Theme.of(context).textTheme.headline4,
                  fontSize: 20,
                  color: Colors.white,
                ),
              ),
            ),
          ]),
          Row(children: [
            Padding(
              padding: EdgeInsets.only(left: 40),
              child: Text(
                "Carrera 9 # 86-50, Apt. 601",
                textAlign: TextAlign.end,
                style: GoogleFonts.raleway(
                  textStyle: Theme.of(context).textTheme.headline4,
                  fontSize: 13,
                  color: Colors.white,
                ),
              ),
            ),
          ]),
          Container(
              width: size.width * 0.8,
              child: Divider(
                color: kPrimaryColor,
                height: 30,
                thickness: 1.3,
              )),
          ServiciosSeleccionadosAlfred(),
          CheckerConductorAlfred(),
          FinalRequestTotal(iva: "", subtotal: "", total: "Total: 29.900"),
          RoundedButton(
            text: "Finalizar",
            press: () {},
          ),
        ],
      ),
    );
  }
}

class VehiculoService extends StatelessWidget {
  final String placa;
  final String info_carro;

  const VehiculoService({
    Key? key,
    required this.placa,
    required this.info_carro,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 1),
      alignment: Alignment.topLeft,
      child: Column(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Container(
            color: kSecondaryLightColor,
            width: size.width * 0.4,
            height: size.height * 0.07,
            alignment: Alignment.center,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text(
                info_carro,
                style: GoogleFonts.raleway(
                    textStyle: Theme.of(context).textTheme.bodyMedium,
                    fontSize: 15,
                    color: Colors.white),
              ),
              Text(
                placa,
                style: GoogleFonts.raleway(
                    textStyle: Theme.of(context).textTheme.bodyLarge,
                    fontSize: 15,
                    color: Colors.white),
              ),
            ])),
      ]),
    );
  }
}


class Formulario extends StatefulWidget {
  @override
  FormularioState createState() => FormularioState();
}

class FormularioState extends State<Formulario> {
  final controllerServicio = TextEditingController();

  final user = FirebaseAuth.instance.currentUser;

  String dropdownvalue = '';

  @override
  Widget build(BuildContext context) => Container(
      height: 350.0,
      width: 300.0,
      child: ListView(
        shrinkWrap: true,
        children: [
          TextField(
            controller: controllerServicio,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'city',
            ),
          ),
          Container(
            child: FutureBuilder(
              future: SingletonDAO.getCarros(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                if (snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.data == null) {
                    return Text('no data');
                  } else {
                    var items = snapshot.data.map((items) {
                      return items["plate"];
                    }).toList();
                    print(items);
                    return DropdownButton(
                      value: snapshot.data.length>0 ? snapshot.data[0] : "",
                      icon: const Icon(Icons.keyboard_arrow_down),   
                      items: items.map((String items) {
                        return DropdownMenuItem(
                          value: items,
                          child: Text(items),
                        );
                      }).toList(),
                      // After selecting the desired option,it will
                      // change button value to selected value
                      onChanged: (newValue) {
                        setState(() {
                          dropdownvalue = newValue!.toString();
                        });
                      },
                    );
                  }
                } else if (snapshot.connectionState == ConnectionState.none) {
                  return Text('Error'); // error
                } else {
                  return CircularProgressIndicator(); // loading
                }
              },
            ),
          ),
          /**
          ElevatedButton(
              child: Text("create"),
              onPressed: () {
                MyCarsControler.createCar(
                  controllerCity.text,
                  controllerCity.text,
                  controllerDescription.text,
                  controllerModel.text,
                  controllerName.text,
                  controllerPlate.text,
                );
                Navigator.pop(context);
              }
            )
            */
        ],
      )
    );
}