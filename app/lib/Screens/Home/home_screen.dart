import 'package:app/Screens/Home/components/background.dart';
import 'package:app/Screens/Home/components/main_body.dart';
import 'package:app/Screens/Home/components/mycarrs_body.dart';
import 'package:app/Screens/Home/components/myservices_body.dart';
import 'package:app/constants.dart';
import 'package:flutter/material.dart';

import 'components/requestService_body.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<HomeScreen> {
  int _currentIndex = 0;
  int _general_Index = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Body(child: _general_Index),
// Bottom bar
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add, color: kbackgroundColor),
        onPressed: () {
          setState(() {
            _general_Index = 4;
          });
        },
        backgroundColor: kPrimaryColor,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          // ignore: prefer_const_literals_to_create_immutables
          items: [
            // ignore: prefer_const_constructors
            BottomNavigationBarItem(
                icon: Icon(Icons.home),
                label: "",
                backgroundColor: Colors.white),
            const BottomNavigationBarItem(
                icon: Padding(
                  padding: EdgeInsets.only(right: 45),
                  child: Icon(Icons.directions_car),
                ),
                label: "",
                backgroundColor: Colors.white),
            const BottomNavigationBarItem(
                icon: Padding(
                  padding: EdgeInsets.only(left: 45),
                  child: Icon(Icons.car_crash),
                ),
                label: "",
                backgroundColor: Colors.white),
            const BottomNavigationBarItem(
                icon: Icon(Icons.menu),
                label: "",
                backgroundColor: Colors.white),
          ],
          onTap: (index) {
            setState(() {
              _currentIndex = index;
              setState(() {
                _general_Index = index;
              });
            });
          },
          type: BottomNavigationBarType.fixed),
    );
  }
}

class Body extends StatelessWidget {
  final int child;
  const Body({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    if (child == 0) {
      return Main_Body();
    }
    if (child == 1) {
      return Cars_Body();
    }
    if (child == 2) {
      return Services_Body();
    }
    if (child == 4) {
      return RequestService_Body();
    }
    return Background(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[],
      ),
    );
  }
}
