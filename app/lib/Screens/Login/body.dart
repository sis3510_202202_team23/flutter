import 'package:app/Controlers/login.dart';
import 'package:app/Screens/Home/home_screen.dart';
import 'package:app/Screens/SignUp/signup_screen.dart';
import 'package:app/dao/singletonDTO.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import '../../components/RoundedButton.dart';
import '../Login/background.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:app/constants.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final TextEditingController emailController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();

    final formkey = GlobalKey<FormState>();

    return Background(
      child: Form(
        key: formkey,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Iniciar sesión",
                style: GoogleFonts.raleway(
                  textStyle: Theme.of(context).textTheme.headline4,
                  fontSize: 20,
                ),
              ),
              SizedBox(height: size.height * 0.03),
              TextFieldContainer(
                child: TextFormField(
                  controller: emailController,
                  decoration: const InputDecoration(labelText: "Correo"),
                ),
              ),
              SizedBox(height: size.height * 0.03),
              TextFieldContainer(
                child: TextFormField(
                  controller: passwordController,
                  obscureText: true,
                  decoration: const InputDecoration(labelText: "Contraseña"),
                ),
              ),
              SizedBox(height: size.height * 0.03),
              RoundedButton(
                  text: "Iniciar Sesión",
                  background: kPrimaryColor,
                  textColor: Colors.black,
                  press: () {
                    print(emailController.text);

                    LoginControler.signIn(emailController.text,
                        passwordController.text, formkey, context);
                  }),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                // ignore: prefer_const_literals_to_create_immutables
                children: <Widget>[
                  // ignore: prefer_const_constructors
                  Text(
                    "¿No tienes una cuenta?",
                    // ignore: prefer_const_constructors
                    style: TextStyle(color: kbackgroundColor),
                  ),
                  // ignore: prefer_const_constructors
                  GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) {
                            return SignUpScreen();
                          },
                        ),
                      );
                    },
                    // ignore: prefer_const_constructors
                    child: Text(
                      "Registrate Aquí",
                      // ignore: prefer_const_constructors
                      style: TextStyle(
                          color: kbackgroundColor, fontWeight: FontWeight.bold),
                    ),
                  )
                ],
              ),
            ]),
      ),
    );
  }
}

class TextContainer extends StatelessWidget {
  final Widget child;
  const TextContainer({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.6,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
      ),
      child: child,
    );
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 2),
      width: size.width * 0.7,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
      ),
      child: child,
    );
  }
}
