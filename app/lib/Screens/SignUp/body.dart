import 'package:app/Controlers/signup.dart';
import 'package:app/dao/singletonDTO.dart';
import 'package:flutter/material.dart';
import '../../components/RoundedButton.dart';
import '../Login/background.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:app/constants.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    final formKey = GlobalKey<FormState>();

    final TextEditingController nombreController = TextEditingController();
    final TextEditingController numeroTelefonoController =
        TextEditingController();
    final TextEditingController emailController = TextEditingController();
    final TextEditingController passwordController = TextEditingController();
    final TextEditingController confirmPasswordController =
        TextEditingController();

    return Background(
      child: Form(
        key: formKey,
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                "Registro",
                style: GoogleFonts.raleway(
                  textStyle: Theme.of(context).textTheme.headline4,
                  fontSize: 20,
                ),
              ),
              SizedBox(height: size.height * 0.01),
              TextFieldContainer(
                child: TextFormField(
                  controller: nombreController,
                  decoration: const InputDecoration(labelText: "Nombre"),
                ),
              ),
              SizedBox(height: size.height * 0.01),
              TextFieldContainer(
                child: TextFormField(
                  controller: emailController,
                  decoration: const InputDecoration(labelText: "Correo"),
                ),
              ),
              TextFieldContainer(
                child: TextFormField(
                  controller: numeroTelefonoController,
                  keyboardType: TextInputType.number,
                  decoration:
                      const InputDecoration(labelText: "Numero Telefonico"),
                ),
              ),
              SizedBox(height: size.height * 0.01),
              TextFieldContainer(
                child: TextFormField(
                  controller: passwordController,
                  obscureText: true,
                  decoration: const InputDecoration(labelText: "Contraseña"),
                ),
              ),
              SizedBox(height: size.height * 0.01),
              TextFieldContainer(
                child: TextFormField(
                  controller: confirmPasswordController,
                  obscureText: true,
                  validator: (value) {
                    if (passwordController.text !=
                        confirmPasswordController.text) {
                      return "La contraseña no coincide";
                    }
                    return null;
                  },
                  decoration:
                      const InputDecoration(labelText: "Confirmar contraseña"),
                ),
              ),
              SizedBox(height: size.height * 0.01),
              RoundedButton(
                  text: "Crear Cuenta",
                  background: kPrimaryColor,
                  textColor: Colors.black,
                  press: () {
                    SignUpControler.signUp(
                        emailController.text,
                        passwordController.text,
                        nombreController.text,
                        numeroTelefonoController.text,
                        formKey,
                        context);
                  }),
            ]),
      ),
    );
  }
}

class TextContainer extends StatelessWidget {
  final Widget child;
  const TextContainer({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      width: size.width * 0.6,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
      ),
      child: child,
    );
  }
}

class TextFieldContainer extends StatelessWidget {
  final Widget child;
  const TextFieldContainer({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 2),
      width: size.width * 0.7,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
      ),
      child: child,
    );
  }
}
