// ignore_for_file: deprecated_member_use, prefer_const_constructors

import 'package:app/Screens/Login/login_screen.dart';
import 'package:app/Screens/SignUp/signup_screen.dart';
import 'package:app/components/LogoImage.dart';
import 'package:app/constants.dart';
import 'package:flutter/material.dart';

import 'package:app/Screens/splash/components/background.dart';
import 'package:app/components/RoundedButton.dart';

class Body extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Background(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          LogoImage(height_: 250, width_: 250),
          SizedBox(height: size.height * 0.03),
          RoundedButton(
              text: "Log In",
              background: kPrimaryColor,
              textColor: kbackgroundColor,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return LoginScreen();
                    },
                  ),
                );
              }),
          SizedBox(height: size.height * 0.03),
          RoundedButton(
              text: "Sign Up",
              background: kSecondaryLightColor,
              textColor: Colors.white,
              press: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return SignUpScreen();
                    },
                  ),
                );
              }),
        ],
      ),
    );
  }
}
