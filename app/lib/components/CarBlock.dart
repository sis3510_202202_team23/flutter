import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../constants.dart';

class CarBlock extends StatelessWidget {

  final String name_;
  final String plate_;
  final String brand_;
  final String model_;
  final String year_;
  final String mileage_;

  const CarBlock({
    Key? key,
    required this.name_,
    required this.plate_,
    required this.brand_,
    required this.model_,
    required this.year_,
    required this.mileage_
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300.0,
      color: kSecondaryLightColor,
      margin: EdgeInsets.only(
        left: 10,
        right: 10,
        top: 10,
        bottom: 10,
      ),
      padding: const EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: 
                [
                  PicoYPlacaWarning2(placa: plate_),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 10),
                        padding: EdgeInsets.all(8.0),
                        decoration: BoxDecoration(
                          border: Border.all(color: Colors.black),
                          color: Colors.yellow,
                          borderRadius: BorderRadius.all(Radius.circular(5))
                        ),
                        child: Text(
                          "$plate_",
                          textAlign: TextAlign.end,
                          style: GoogleFonts.raleway(
                            textStyle: Theme.of(context).textTheme.headline4,
                            fontSize: 15,
                            color: Colors.black,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ),
                      Image.asset(
                        "assets/images/carrito2.png",
                        height: 100,
                        width: 100
                      ),
                      Text(
                        "$name_",
                        textAlign: TextAlign.end,
                        style: GoogleFonts.raleway(
                          textStyle: Theme.of(context).textTheme.headline4,
                          fontSize: 20,
                          color: Colors.white,
                          fontWeight: FontWeight.w700
                        ),
                      )
                    ],
                ),
            ],
          ),
          
          
        ],
      ),  
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: Text(
                          "Placa: $plate_",
                          textAlign: TextAlign.end,
                          style: GoogleFonts.raleway(
                            textStyle: Theme.of(context).textTheme.headline4,
                            fontSize: 15,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: Text(
                          "Marca: $brand_",
                          textAlign: TextAlign.end,
                          style: GoogleFonts.raleway(
                            textStyle: Theme.of(context).textTheme.headline4,
                            fontSize: 15,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: Text(
                          "Modelo: $model_",
                          textAlign: TextAlign.end,
                          style: GoogleFonts.raleway(
                            textStyle: Theme.of(context).textTheme.headline4,
                            fontSize: 15,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: Text(
                          "Year: $year_",
                          textAlign: TextAlign.end,
                          style: GoogleFonts.raleway(
                            textStyle: Theme.of(context).textTheme.headline4,
                            fontSize: 15,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.all(8),
                        child: Text(
                          "Kilometraje: $mileage_",
                          textAlign: TextAlign.end,
                          style: GoogleFonts.raleway(
                            textStyle: Theme.of(context).textTheme.headline4,
                            fontSize: 15,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ); 
  }
}

class PicoYPlacaWarning2 extends StatelessWidget {
  final String placa;
  const PicoYPlacaWarning2({Key? key, required this.placa}) : super(key: key);  
  @override
  Widget build(BuildContext context) {
    int hoy = DateTime.now().day;
    int ultimoDigito = int.parse(placa.substring(placa.length -1));
    if(ultimoDigito%2 == hoy%2){
      return Container(
        child: Text(
          "Pico y Placa",
          style: GoogleFonts.raleway(
          textStyle: Theme.of(context).textTheme.headline4,
          fontSize: 15,
          color: Colors.red,
          fontWeight: FontWeight.w700
          ),  
        ),
      );
    }
    else {
      return Container(
        child: Text(
          "   ",
          style: GoogleFonts.raleway(
          textStyle: Theme.of(context).textTheme.headline4,
          fontSize: 15,
          color: Colors.red,
          fontWeight: FontWeight.w700
          ),  
        ),
      );
    }
  }
}