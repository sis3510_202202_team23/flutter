import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../../constants.dart';

class CarCard extends StatelessWidget {

  final String name_;
  final String plate_;

  const CarCard({
    Key? key,
    required this.name_,
    required this.plate_,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 150.0,
      color: kSecondaryLightColor,
      margin: const EdgeInsets.only(left: 10,right: 10,top: 5,bottom: 5),
      padding: const EdgeInsets.all(8),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              PicoYPlacaWarning2(placa: plate_),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Container(
                    margin: EdgeInsets.only(top: 10),
                    padding: EdgeInsets.all(8.0),
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black),
                      color: Colors.yellow,
                      borderRadius: BorderRadius.all(Radius.circular(5))
                    ),
                    child: Text(
                      "$plate_",
                      textAlign: TextAlign.end,
                      style: GoogleFonts.raleway(
                        textStyle: Theme.of(context).textTheme.headline4,
                        fontSize: 15,
                        color: Colors.black,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                  ),
                  Image.asset(
                    "assets/images/carrito2.png",
                    height: 100,
                    width: 100
                  ),
                  Text(
                    "$name_",
                    textAlign: TextAlign.end,
                    style: GoogleFonts.raleway(
                      textStyle: Theme.of(context).textTheme.headline4,
                      fontSize: 20,
                      color: Colors.white,
                      fontWeight: FontWeight.w700
                    ),
                  )
                ],
              ),
            ],
          ),
          
          
        ],
      ),    
    ); 
  }
}

class PicoYPlacaWarning2 extends StatelessWidget {
  final String placa;
  const PicoYPlacaWarning2({Key? key, required this.placa}) : super(key: key);  
  @override
  Widget build(BuildContext context) {
    int hoy = DateTime.now().day;
    int ultimoDigito = int.parse(placa.substring(placa.length -1));
    if(ultimoDigito%2 == hoy%2){
      return Container(
        child: Text(
          "Pico y Placa",
          style: GoogleFonts.raleway(
          textStyle: Theme.of(context).textTheme.headline4,
          fontSize: 15,
          color: Colors.red,
          fontWeight: FontWeight.w700
          ),  
        ),
      );
    }
    else {
      return Container(
        child: Text(
          "   ",
          style: GoogleFonts.raleway(
          textStyle: Theme.of(context).textTheme.headline4,
          fontSize: 15,
          color: Colors.red,
          fontWeight: FontWeight.w700
          ),  
        ),
      );
    }
  }
}