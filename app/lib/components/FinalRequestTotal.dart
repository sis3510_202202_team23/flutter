import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';

import 'package:app/constants.dart';

class FinalRequestTotal extends StatelessWidget {
  final String subtotal;
  final String iva;
  final String total;
  const FinalRequestTotal({
    Key? key,
    required this.subtotal,
    required this.iva,
    required this.total,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 0, vertical: 5),
        width: size.width * 0.8,
        // ignore: prefer_const_constructors
        child: Padding(
          padding: const EdgeInsets.only(
            top: 1,
            bottom: 1,
          ),
          child: Container(
            alignment: Alignment.topLeft,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Divider(
                color: kPrimaryColor,
                height: 30,
                thickness: 1.3,
              ),
              Text(
                subtotal,
                style: GoogleFonts.raleway(
                  color: Colors.white,
                  textStyle: Theme.of(context).textTheme.bodySmall,
                  fontSize: 15,
                ),
              ),

              Container(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    iva,
                    style: GoogleFonts.raleway(
                      color: Colors.white,
                      textStyle: Theme.of(context).textTheme.bodyMedium,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  alignment: Alignment.topRight,
                  child: Text(
                    total,
                    // ignore: prefer_const_constructors
                    style: GoogleFonts.raleway(
                      color: Colors.white,
                      textStyle: Theme.of(context).textTheme.bodyMedium,
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
              // ignore: prefer_const_constructors
            ]),
          ),
        ));
  }
}
