import 'package:flutter/material.dart';

class LogoImage extends StatelessWidget {
  final double height_;
  final double width_;
  const LogoImage({
    Key? key,
    required this.height_,
    required this.width_,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: height_,
      width: width_,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: ExactAssetImage('assets/images/alfred_main_logo.png'),
        ),
        shape: BoxShape.circle,
      ),
    );
  }
}
