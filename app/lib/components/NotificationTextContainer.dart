import 'package:flutter/material.dart';

import 'package:google_fonts/google_fonts.dart';

import 'package:app/constants.dart';

class NotificationTextContainer extends StatelessWidget {
  final String service_status;
  final String service_type;
  final String fecha;
  const NotificationTextContainer({
    Key? key,
    required this.service_status,
    required this.service_type,
    required this.fecha,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 0, vertical: 5),
        width: size.width * 0.8,
        // ignore: prefer_const_constructors
        child: Padding(
          padding: const EdgeInsets.only(
            top: 1,
            bottom: 1,
          ),
          child: Container(
            alignment: Alignment.topLeft,
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              Text(
                service_status,
                style: GoogleFonts.raleway(
                  color: Colors.white,
                  textStyle: Theme.of(context).textTheme.bodySmall,
                  fontSize: 15,
                ),
              ),
              SizedBox(height: size.height * 0.01),
              Container(
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    service_type,
                    maxLines: 3,
                    textAlign: TextAlign.end,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                    ),
                  ),
                ),
              ),
              Container(
                child: Align(
                  alignment: Alignment.topRight,
                  child: Text(
                    fecha,
                    maxLines: 3,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 15,
                    ),
                  ),
                ),
              ),
              Divider(
                color: kPrimaryColor,
                height: 30,
                thickness: 2,
              ),
            ]),
          ),
        ));
  }
}
