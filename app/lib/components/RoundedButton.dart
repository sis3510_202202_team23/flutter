import 'package:app/constants.dart';
import 'package:flutter/material.dart';

import 'package:app/Screens/splash/components/background.dart';
import 'package:google_fonts/google_fonts.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final Function()? press;
  final Color background, textColor;
  const RoundedButton({
    Key? key,
    required this.text,
    required this.press,
    this.background = kPrimaryColor,
    this.textColor = kbackgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.symmetric(vertical: 10),
      width: size.width * 0.7,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(20),
        child: TextButton(
          onPressed: press,
          style: ButtonStyle(backgroundColor: MaterialStateProperty.all(background)),
          child: Text(
            text,
            style: GoogleFonts.raleway(
                textStyle: Theme.of(context).textTheme.headline4,
                fontSize: 20,
                color: textColor),
          ),
        ),
      ),
    );
  }
}
