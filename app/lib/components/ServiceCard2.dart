import 'dart:developer';
import 'package:google_fonts/google_fonts.dart';
import 'package:flutter/material.dart';
import '../../../constants.dart';

class ServiceCard2 extends StatelessWidget {
  final String nombreservicio_;
  final String nombrefoto_;
  final Color color_;
  final bool recomended;

  const ServiceCard2({
    Key? key,
    required this.nombreservicio_,
    required this.nombrefoto_,
    required this.color_,
    required this.recomended,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: 120.0,
        color: color_,
        margin: const EdgeInsets.only(left: 10, right: 10, top: 5, bottom: 5),
        padding: const EdgeInsets.all(8),
        child: isRecomended(context));
  }

  Column isRecomended(BuildContext context) {
    if (recomended) {
      return Column(children: [
        Text(
          "recomendado",
          style: GoogleFonts.raleway(
              textStyle: Theme.of(context).textTheme.bodyMedium,
              fontSize: 10,
              color: Colors.white,
              fontWeight: FontWeight.bold),
        ),
        Image.asset("assets/images/" + nombrefoto_ + ".png",
            height: 75, width: 75),
        Text(
          nombreservicio_,
          style: GoogleFonts.raleway(
              textStyle: Theme.of(context).textTheme.headline4,
              fontSize: 15,
              color: Colors.white,
              fontWeight: FontWeight.w400),
        ),
      ]);
    } else {
      return Column(children: [
        Image.asset("assets/images/" + nombrefoto_ + ".png",
            height: 75, width: 75),
        Text(
          nombreservicio_,
          style: GoogleFonts.raleway(
              textStyle: Theme.of(context).textTheme.headline4,
              fontSize: 15,
              color: Colors.white,
              fontWeight: FontWeight.w400),
        ),
      ]);
    }
  }
}
