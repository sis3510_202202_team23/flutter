import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class CheckerConductorAlfred extends StatefulWidget {
  @override
  _CheckerConductorAlfred createState() => _CheckerConductorAlfred();
}

class _CheckerConductorAlfred extends State<CheckerConductorAlfred> {
  bool? checked = false;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 1),
      alignment: Alignment.topLeft,
      child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
        Text(
          "Con conductor Alfred",
          style: GoogleFonts.raleway(
              textStyle: Theme.of(context).textTheme.bodySmall,
              fontSize: 15,
              color: Colors.white),
        ),
        Theme(
            data: ThemeData(
              unselectedWidgetColor: Colors.white,
            ),
            child: Checkbox(
                value: checked,
                onChanged: (value) {
                  setState(() {
                    checked = value;
                  });
                }))
      ]),
    );
  }
}
