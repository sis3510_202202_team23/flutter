import 'package:flutter/material.dart';
import 'package:app/constants.dart';
import 'package:google_fonts/google_fonts.dart';

class ServiciosSeleccionadosAlfred extends StatelessWidget {
  const ServiciosSeleccionadosAlfred({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      width: size.width * 0.8,
      padding: EdgeInsets.symmetric(horizontal: 0, vertical: 1),
      alignment: Alignment.topLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            "Servicios Seleccionados:",
            style: GoogleFonts.raleway(
                textStyle: Theme.of(context).textTheme.bodyLarge,
                fontSize: 15,
                color: Colors.white),
          ),
          SizedBox(height: size.height * 0.01),
          Container(
            color: kSecondaryLightColor,
            width: size.width * 0.4,
            height: size.height * 0.05,
            alignment: Alignment.centerLeft,
            child: Text(
              " Lavado alfred",
              style: GoogleFonts.raleway(
                  textStyle: Theme.of(context).textTheme.bodySmall,
                  fontSize: 15,
                  color: Colors.white),
            ),
          )
        ],
      ),
    );
  }
}
