import 'package:flutter/material.dart';

const kPrimaryColor = Color.fromARGB(255, 20, 235, 0);
const kPrimarySoftColor = Color.fromARGB(255, 110, 225, 99);

const kbackgroundColor = Color.fromARGB(255, 17, 30, 62);
const kPrimaryLightColor = Color(0xFFF1E6FF);
const kSecondaryLightColor = Color.fromARGB(255, 48, 54, 78);
const double defaultPadding = 16.0;
