import 'package:app/components/CarCard.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class CarsDAO extends StatelessWidget {
  static Future getCarros() async {
    final user = FirebaseAuth.instance.currentUser;

    var firestore = FirebaseFirestore.instance;
    QuerySnapshot querySnapshot = await firestore
        .collection("cars")
        .where("user", isEqualTo: user!.uid)
        .get();
    final documents = querySnapshot.docs;
    return documents;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
