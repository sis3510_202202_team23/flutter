import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class MyServicesDAO extends StatelessWidget {
  static Future getMyServices() async {
    final user = FirebaseAuth.instance.currentUser;

    var firestore = FirebaseFirestore.instance;
    QuerySnapshot querySnapshot = await firestore
        .collection("services")
        .where("idUser", isEqualTo: user!.uid)
        .get();
    final documents = querySnapshot.docs;
    return documents;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
