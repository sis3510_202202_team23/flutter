import 'package:app/components/CarCard.dart';
import 'package:app/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../components/ServiceCard2.dart';

class ServiciosDAO extends StatelessWidget {
  static Future getServicioOrdenados() async {
    final user = FirebaseAuth.instance.currentUser;
    var firestore = FirebaseFirestore.instance;
    QuerySnapshot querySnapshot = await firestore
        .collection("servicetypes")
        .orderBy("amount", descending: true)
        .get();
    final documents = querySnapshot.docs;
    //código para organizar el documento
    return documents;
  }

  static Future getServiciosFavoritos() async {
    final user = FirebaseAuth.instance.currentUser;
    var firestore = FirebaseFirestore.instance;
    QuerySnapshot querySnapshot = await firestore
        .collection("services_clients")
        .where("idUser", isEqualTo: user!.uid)
        .get();
    final documents = querySnapshot.docs;
    //código para organizar el documento
    return documents;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
