import 'package:app/components/CarCard.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:app/dao/cars.dart';
import 'package:app/dao/myservices.dart';

import 'package:app/dao/services.dart';
import 'package:app/dao/users.dart';

class SingletonDAO extends StatelessWidget {
  static Future getCarros() async {
    return CarsDAO.getCarros();
  }

  static Future getMyServices() async {
    return MyServicesDAO.getMyServices();
  }

  static Future getServiciosFavoritos() async {
    return ServiciosDAO.getServiciosFavoritos();
  }

  static Future getServicioOrdenados() async {
    return ServiciosDAO.getServicioOrdenados();
  }

  static Future signUp(String email, String password, String nombre,
      String numero, GlobalKey<FormState> formkey, BuildContext context) async {
    return UsersDAO.signUp(email, password, nombre, numero, formkey, context);
  }

  static getUser() {
    return UsersDAO.getUser();
  }

  static void signIn(String email, String password,
      GlobalKey<FormState> formkey, BuildContext context) async {
    UsersDAO.signIn(email, password, formkey, context);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}
