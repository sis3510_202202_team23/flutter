import 'package:flutter/material.dart';

import 'package:app/constants.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:app/Screens/Home/home_screen.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../../model/user.dart' as model;

class UsersDAO extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }

  static getUser() {
    return FirebaseAuth.instance.currentUser;
  }

  static void signIn(String email, String password,
      GlobalKey<FormState> formkey, BuildContext context) async {
    final auth = FirebaseAuth.instance;
    String errorMessage;
    if (formkey.currentState!.validate()) {
      try {
        auth
            .signInWithEmailAndPassword(email: email, password: password)
            .then((uid) => {
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => HomeScreen())),
                });
      } on FirebaseAuthException catch (error) {
        switch (error.code) {
          case "invalid-email":
            errorMessage =
                "Parece que el correo que ingresaste está malformado";
            break;
          case "wrong-password":
            errorMessage = "La contraseña es incorrecta";
            break;
          case "user-not-found":
            errorMessage = "No existe un usuario con el correo ingresado";
            break;
          case "user-disabled":
            errorMessage = "El usuaio con este correo fue deshabilitado";
            break;
          case "too-many-requests":
            errorMessage = "Muchas peticiones. Intenta de nuevo más tarde";
            break;
          case "operation-not-allowed":
            errorMessage =
                "No se puede iniciar sesión con correo y contraseña en este momento";
            break;
          default:
            errorMessage = "An undefined Error happened.";
        }
        Fluttertoast.showToast(msg: errorMessage);
      }
    }
  }

  static void signUp(String email, String password, String nombre,
      String numero, GlobalKey<FormState> formkey, BuildContext context) async {
    final auth = FirebaseAuth.instance;
    if (formkey.currentState!.validate()) {
      await auth
          .createUserWithEmailAndPassword(email: email, password: password)
          .then((value) {
        postDetailsForFirestore(context, nombre, numero);
      }).catchError((e) {
        Fluttertoast.showToast(msg: e!.message);
      });
    }
  }

  static postDetailsForFirestore(
      BuildContext context, String nombre, String numeroTelefonico) async {
    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    //call userModel
    final auth = FirebaseAuth.instance;
    User? user = auth.currentUser;

    model.UserModel userModel = model.UserModel();

    userModel.email = user!.email;
    userModel.cellphone = int.parse(numeroTelefonico);
    userModel.uid = user.uid;
    userModel.name = nombre;

    await firebaseFirestore
        .collection("users")
        .doc(user.uid)
        .set(userModel.toMap());
    Fluttertoast.showToast(msg: "Usuario creado exitosamente");
    // ignore: use_build_context_synchronously
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => HomeScreen()));
  }
}
