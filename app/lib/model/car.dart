import 'package:cloud_firestore/cloud_firestore.dart';

class CarModel {
  String? city;
  String? country;
  String? description;
  String? model;
  String? name;
  String? plate;
  String? user;

  CarModel({
    this.city, 
    this.country, 
    this.description, 
    this.model, 
    this.name,
    this.plate,
    this.user,
  });

  //receive data from server
  factory CarModel.fromMap(map) {
    return CarModel(
      city: map['city'],
      country: map['country'],
      description: map['description'],
      model: map['model'],
      name: map['name'],
      plate: map['plate'],
      user: map['user'],
    );
  }

  //sending data to server
  Map<String, dynamic> toMap() {
    return {
      'city': city,
      'country': country,
      'description': description,
      'model': model,
      'name': name,
      'plate': plate,
      'user': user
    };
  }
}
