import 'package:cloud_firestore/cloud_firestore.dart';

class ServiceModel {
  String? idCar;
  String? idUser;
  String? serviceType;
  String? state;

  ServiceModel({
    this.idCar,
    this.idUser,
    this.serviceType,
    this.state,
  });

  //receive data from server
  factory ServiceModel.fromMap(map) {
    return ServiceModel(
      idCar: map['idCar'],
      idUser: map['idUser'],
      serviceType: map['serviceType'],
      state: map['state'],
    );
  }

  //sending data to server
  Map<String, dynamic> toMap() {
    return {
      'idCar': idCar,
      'idUser': idUser,
      'serviceType': serviceType,
      'state': state,
    };
  }
}
