class UserModel {
  String? uid;
  String? email;
  String? name;
  String? photoUrl;
  int? cellphone;

  UserModel({this.uid, this.email, this.cellphone, this.name, this.photoUrl});

  //receive data from server
  factory UserModel.fromMap(map) {
    return UserModel(
      uid: map['uid'],
      email: map['email'],
      name: map['name'],
      cellphone: map['cellphone'],
    );
  }

  //sending data to server
  Map<String, dynamic> toMap() {
    return {
      'uid': uid,
      'email': email,
      'cellphone': cellphone,
      'name': name,
    };
  }
}
